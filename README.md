#README 

A partir d'un password PassPot.py est un script qui génère un dictionnaire de passwords similaires en se basant sur le fait qu'une grande partie des utilisateurs utilisent le même password légèrement modifié.

Pour tester le script sur son propre password il suffit de la lancer, rentrer son password et ensuite faire un grep de ses passwords similaires sur le dico.
